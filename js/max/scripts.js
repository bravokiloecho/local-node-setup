$(function() {
  var $body, $document, $html, $window, HEIGHT, WIDTH, debouncedResize, isDevice, isMobile, isTablet, isTouch, onKeyPress, onResize, svgFallback, throttledResize;
  svgFallback = function(svgSupport) {
    if (!svgSupport) {
      return $('img[src*="svg"]').attr('src', function() {
        return $(this).attr('src').replace('.svg', '.png');
      });
    }
  };
  onKeyPress = function(keyCode, callback, argument) {
    return $window.on('keyup', function(e) {
      if (keyCode === 'space') {
        keyCode = 32;
      } else if (keyCode === 'right') {
        keyCode = 39;
      } else if (keyCode === 'left') {
        keyCode = 37;
      } else if (keyCode === 'up') {
        keyCode = 39;
      } else if (keyCode === 'down') {
        keyCode = 40;
      } else {
        keyCode === keyCode;
      }
      if (e.keyCode === keyCode) {
        return callback(argument);
      }
    });
  };
  $window = $(window);
  $document = $(document);
  $html = $('html');
  $body = $('body');
  WIDTH = $window.width();
  HEIGHT = $window.height();
  isMobile = $('#mobileTester').css('visibility') === 'visible' ? true : false;
  isTablet = $('#tabletTester').css('visibility') === 'visible' ? true : false;
  isDevice = isMobile || isTablet ? true : false;
  isTouch = Modernizr.touch ? true : false;
  if (isDevice) {
    $html.addClass('device');
    if (isTablet) {
      $html.addClass('tablet');
    }
    if (isMobile) {
      $html.addClass('mobile');
    }
  }
  onResize = function() {
    WIDTH = $window.width();
    return HEIGHT = $window.height();
  };
  throttledResize = $.throttle(250, function() {
    return console.log('throttledResize');
  });
  debouncedResize = $.debounce(100, function() {
    return console.log('debouncedResize');
  });
  debouncedResize();
  $window.on('resize', onResize);
  $window.on('resize', throttledResize);
  $window.on('resize', debouncedResize);
  $window.trigger("resize");
  FastClick.attach(document.body);
  PointerEventsPolyfill.initialize({});
  return svgFallback(Modernizr.svg);
});
