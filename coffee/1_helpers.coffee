	
	# SWAP SVGs with PNGs
	svgFallback = (svgSupport) ->
		if !svgSupport
			$('img[src*="svg"]').attr 'src', ->
				$(this).attr('src').replace('.svg', '.png')

	onKeyPress = (keyCode , callback , argument) ->
		$window.on 'keyup' , (e) ->
			
			# a few special cases, else defaults to value given
			if keyCode is 'space' then keyCode = 32
			else if keyCode is 'right' then keyCode = 39
			else if keyCode is 'left' then keyCode = 37
			else if keyCode is 'up' then keyCode = 39
			else if keyCode is 'down' then keyCode = 40
			else keyCode is keyCode

			if e.keyCode == keyCode
				callback(argument)