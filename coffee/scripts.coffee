	# BASIC VARIABLES
	# ###############
	$window = $(window)
	$document = $(document)
	$html = $ 'html'
	$body = $ 'body'
	WIDTH = $window.width()
	HEIGHT = $window.height()

	isMobile = if $('#mobileTester').css('visibility') == 'visible' then true else false
	isTablet = if $('#tabletTester').css('visibility') == 'visible' then true else false
	isDevice = if isMobile or isTablet then true else false
	isTouch = if Modernizr.touch then true else false

	if isDevice
		$html.addClass 'device'
		if isTablet
			$html.addClass 'tablet'
		if isMobile
			$html.addClass 'mobile'

	# console.log 'isMobile: '+isMobile
	# console.log 'isTablet: '+isTablet
	# console.log 'isDevice: '+isDevice
	# console.log 'isTouch: '+isTouch

	# GLOBAL VARIABLES
	# ################


	# RESIZE SCRIPTS
	# ##############

	onResize = ->
		WIDTH = $window.width()
		HEIGHT = $window.height()
		# fix for minimal-ui glitch on iPhones
		# only use if no scrolling on site
		# window.scrollTo(0, 0)

	throttledResize = $.throttle 250 , ->
		# do throttled things
		console.log 'throttledResize'

	debouncedResize = $.debounce 100 , ->
		# do debounced things
		console.log 'debouncedResize'

	do debouncedResize

	$window.on 'resize' , onResize
	$window.on 'resize' , throttledResize
	$window.on 'resize' , debouncedResize

	$window.trigger "resize"

	
	# ACTIVATE HELPER PLUGINS
	# #######################
	
	# FASTCLICK for touch devices 
	FastClick.attach(document.body)
	# Pointer events polyfill for IE
	PointerEventsPolyfill.initialize({})
	# svg fallback
	svgFallback Modernizr.svg