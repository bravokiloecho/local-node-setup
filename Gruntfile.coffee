module.exports = (grunt) ->
  
  # Project configuration.
  grunt.initConfig
    pkg: grunt.file.readJSON("package.json")
    
    # DEFINE PATH TO ASSET FOLDER
    # leave blank if in same folder as gruntfile
    asset_path: ''

    watch:
      sass:
        files: [
          "<%= asset_path %>sass/*"
          "<%= asset_path %>sass/*/*"
        ]
        tasks: ["compass:dev"]

      coffee:
        files: ["<%= asset_path %>coffee/*"]
        tasks: ["<%= asset_path %>coffee"]

      watchFiles:
        files: [
          "<%= asset_path %>js/max/*"
          "<%= asset_path %>*.php"
          "<%= asset_path %>partials/*"
        ]
        options:
          livereload: true

    browserSync:
      options:
        watchTask: true

      files:
        src: [
          "<%= asset_path %>css/max/*.css"
          "<%= asset_path %>gfx/*"
        ]

    uglify:
      plugins:
        options:
          mangle: true
          beautify: false
          compress: true

        files:
          "<%= asset_path %>js/plugins.js": ["<%= asset_path %>plugins/*.js"]

      
      # combines plugins and scripts
      combine_all:
        files:
          "<%= asset_path %>js/min/scripts.js": [
            "<%= asset_path %>js/plugins.js"
            "<%= asset_path %>js/max/scripts.js"
          ]

    imagemin: # Task
      dynamic: # Another target
        options:
          cache: false
          optimizationLevel: 2

        files: [
          expand: true # Enable dynamic expansion
          cwd: "<%= asset_path %>gfx/" # Src matches are relative to this path
          src: ["<%= asset_path %>**/*.{png,jpg,gif}"] # Actual patterns to match
          dest: "<%= asset_path %>gfx_min/" # Destination path prefix
        ]

    compass:
      dev:
        options:
          sassDir: "<%= asset_path %>sass"
          cssDir: "<%= asset_path %>css/max"
          outputStyle: "nested"
          fontsPath: "Fonts"

      dist:
        options:
          sassDir: "<%= asset_path %>sass"
          cssDir: "<%= asset_path %>css/min"
          outputStyle: "compressed"
          fontsPath: "<%= asset_path %>Fonts"

    coffee:
      compileBare:
        options:
          bare: true
          join: true

        files:
          "<%= asset_path %>js/max/scripts.js": ["<%= asset_path %>coffee/*.coffee"]

    webfont:
      icons:
        src: "<%= asset_path %>icons/*.svg"
        dest: "<%= asset_path %>Fonts/icons"
        destCss: "<%= asset_path %>sass/partials"
        options:
          stylesheet: "scss"
          
          # template: 'icons/templates/_icon_template.scss',
          relativeFontPath: "../../Fonts/icons/"
          destHtml: "Fonts/icons/preview"
          ligatures: true
          templateOptions:
            baseClass: "icon"
            classPrefix: "icon-"
            mixinPrefix: "icon-"

    php:
      dist:
        options:
          port: 5000
          open: true

  
  # Load grunt plugins.
  grunt.loadNpmTasks "grunt-contrib-compass"
  grunt.loadNpmTasks "grunt-contrib-watch"
  grunt.loadNpmTasks "grunt-browser-sync"
  grunt.loadNpmTasks "grunt-contrib-uglify"
  grunt.loadNpmTasks "grunt-contrib-coffee"
  grunt.loadNpmTasks "grunt-contrib-imagemin"
  grunt.loadNpmTasks "grunt-webfont"
  grunt.loadNpmTasks 'grunt-php'
  
  #register tasks
  
  #watch with css inject
  grunt.registerTask "default", [
    "php"
    "browserSync"
    "watch"
  ]
  
  # compile all files that need compiling
  grunt.registerTask "c", [
    "compass"
    "coffee"
    "uglify:plugins"
    "uglify:combine_all"
  ]
  
  # minify images
  grunt.registerTask "image", ["imagemin:dynamic"]
  
  # make icon font
  grunt.registerTask "icons", ["webfont"]
  