<!doctype html>

<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class=""> <!--<![endif]-->
<head>
<meta charset="UTF-8" content="IE=edge">

<!--Stop Robots-->
<meta name="robots" content="noindex">
<!-- *** DELETE WHEN READY *** -->

<!-- ADD A SITE DESCRIPTION HERE -->
<!-- <meta name="description" content=""> -->

<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale = 1.0, maximum-scale = 1.0, minimal-ui">


<!-- APPABLE THINGS -->
<!-- <meta name="apple-mobile-web-app-capable" content="yes" />
<link rel="apple-touch-icon-precomposed" href="/gfx/appleIcon.png" />  -->

<!--[if lt IE 9]>
<script src="/js/html5shiv.js"></script>
<![endif]-->


<link rel="icon" href="/favicon.ico" type="image/x-icon">

<title>new site</title>

<?php
$local_ip = '127.0.0.1';
$local = false;
$current_ip = $_SERVER['REMOTE_ADDR'];
$v = time();

if ($current_ip == $local_ip)
	$local = true;

if ($local)
	$assetsPath = 'max';

else
	$assetsPath = 'min';

?>


<?php 
	//CSS
	echo "<link type='text/css' href='/css/".$assetsPath."/index.css?<?=$v?>' rel='stylesheet' media='all' />";
?>

<!--[if lt IE 9]>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="/jquery/jquery_1.js"><\/script>')</script>
<![endif]-->
<!--[if gte IE 9]><!-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="/jquery/jquery_2.js"><\/script>')</script>
<!--<![endif]-->


<?php 
	//JS
	if ($local) {
		echo "
		<script src='/js/plugins.js?<?=$v?>'></script>
		<script src='/js/max/scripts.js?<?=$v?>'></script>
		";
	} else {
		echo "
		<script src='/js/min/scripts.js?<?=$v?>'></script>
		";
	}
?>

</head>